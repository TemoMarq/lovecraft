﻿using UnityEngine;
using Fungus;


namespace TheBitCave.FungusUtils
{
    /// <summary>
    /// attempting to create a "card manager" which will listen to when the mouse is over a card
    /// make that card "selectedCard"
    /// each card has a corresponding scene
    /// when cards are played on characters, they open up a 3rd dialogue option that initiates a special scene
    /// 
    /// </summary>
    public class CardManagerScript: MonoBehaviour
    {

        public Flowchart flowchart;

        /// Most of this is ripped from a example about coding fungus for 3D environment
        /// There are nodes on the flowchart corresponding to each public string
        /// just a way to cut through fungus to debug log
        /// card values are in the title
        public string onMouseDownMessage;
        public string onMouseEnterMessage;
        public string onMouseExitMessage;
        public string onMouseOverMessage;
        public string onMouseUpMessage;
        public string onMouseUpAsButtonMessage;
        private object selectedCard;

        //public GameObject selectedCard;

        void OnMouseDown()
        {
            if (!string.IsNullOrEmpty(onMouseDownMessage))
                flowchart.SendFungusMessage(onMouseDownMessage);
        }

        void OnMouseEnter()
        {
            if (!string.IsNullOrEmpty(onMouseEnterMessage))
                flowchart.SendFungusMessage(onMouseEnterMessage);

            //this = new GameObject selectedCard();
            selectedCard = GetComponent<Rigidbody2D>();
        }

        void OnMouseExit()
        {
            if (!string.IsNullOrEmpty(onMouseExitMessage))
                flowchart.SendFungusMessage(onMouseExitMessage);
        }

        void OnMouseOver()
        {
            if (!string.IsNullOrEmpty(onMouseOverMessage))
                flowchart.SendFungusMessage(onMouseOverMessage);
        }

        void OnMouseUp()
        {
            if (!string.IsNullOrEmpty(onMouseUpMessage))
                flowchart.SendFungusMessage(onMouseUpMessage);
        }

        void OnMouseUpAsButton()
        {
            if (!string.IsNullOrEmpty(onMouseUpAsButtonMessage))
                flowchart.SendFungusMessage(onMouseUpAsButtonMessage);
        }
    }
}
